import { debounceTime } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  constructor(private apiService: ApiService) {}
  public filmName: FormControl;
  public loader: boolean = false;
  public filmsArr = [];

  ngOnInit() {
    this.filmName = new FormControl();
    this.filmName.valueChanges.pipe(debounceTime(400)).subscribe(val => {
      this.showFilms(val);
    });
  }
  public showFilms(name): void {
    this.loader = true;
    this.apiService.getFilms(name).then(data => {
      this.filmsArr = data;
      this.loader = false;
    });
  }
}
